import 'dart:async';
import 'dart:math';
import 'dart:developer' as developer;
import 'dart:math';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Sandbox'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static int passwordLength = 8;
  static String password = "#" * passwordLength;

  static bool useLowercase = true;
  static bool useUppercase = false;
  static bool useNumbers = false;
  static bool useSpecialCharacters = false;

  static final String alphabet = "acdefghijklmnopqrstuvwxyz";
  static final String numbers = "0123456789";
  static final String specialCharacters = "!#\$%&()*+-/=?@[]_{}";

  bool isGeneratingPassword = false;

  void generatePassword() async {
    if (isGeneratingPassword) return;
    if (!isAnyCheckboxSelected())
      return showToast("You must choose at least one option...");

    List<String> charset = getCharsetFromCheckboxesValues();
    final Random random = new Random();

    String generatedPasswordPart = "";
    String placeholder = "#" * passwordLength;
    password = placeholder;

    isGeneratingPassword = true;

    for (int i = 0; i < passwordLength; i++) {
      await Future.delayed(const Duration(milliseconds: 50), () {
        generatedPasswordPart += charset[random.nextInt(charset.length)];
        setState(() {
          password = generatedPasswordPart +
              placeholder.substring(i + 1, placeholder.length);
        });
      });
    }

    isGeneratingPassword = false;
  }

  bool isAnyCheckboxSelected() {
    return useLowercase || useUppercase || useNumbers || useSpecialCharacters;
  }

  void showToast(String text) {
    Fluttertoast.cancel();
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 2,
        backgroundColor: Colors.white70,
        textColor: Colors.black87,
        fontSize: 12.0);
    return;
  }

  List<String> getCharsetFromCheckboxesValues() {
    String charset = "";

    if (useLowercase) charset += alphabet;
    if (useUppercase) charset += alphabet.toUpperCase();
    if (useNumbers) charset += numbers;
    if (useSpecialCharacters) charset += specialCharacters;

    return charset.split("");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Text('Flutter Sandbox'),
                decoration: BoxDecoration(
                  color: Colors.amber,
                ),
              ),
              ListTile(title: Text("Home"), onTap: () {}),
              ListTile(title: Text("Password Generator"), onTap: () {})
            ],
          ),
        ),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              Container(
                  margin: new EdgeInsets.only(bottom: 50),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Password length',
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      Slider(
                        value: passwordLength.toDouble(),
                        min: 4,
                        max: 16,
                        divisions: 16,
                        label: passwordLength.round().toString(),
                        onChanged: (double value) {
                          setState(() {
                            passwordLength = value.toInt();
                            password = "#" * passwordLength;
                          });
                        },
                      )
                    ],
                  )),
              Container(
                  margin: new EdgeInsets.only(bottom: 50),
                  child: Column(children: <Widget>[
                    CheckboxListTile(
                      title: Text('Include lowercase'),
                      value: useLowercase,
                      onChanged: (bool value) {
                        setState(() {
                          useLowercase = value;
                        });
                      },
                    ),
                    CheckboxListTile(
                      title: Text('Include uppercase'),
                      value: useUppercase,
                      onChanged: (bool value) {
                        setState(() {
                          useUppercase = value;
                        });
                      },
                    ),
                    CheckboxListTile(
                      title: Text('Include numbers'),
                      value: useNumbers,
                      onChanged: (bool value) {
                        setState(() {
                          useNumbers = value;
                        });
                      },
                    ),
                    CheckboxListTile(
                      title: Text('Include special characters'),
                      value: useSpecialCharacters,
                      onChanged: (bool value) {
                        setState(() {
                          useSpecialCharacters = value;
                        });
                      },
                    )
                  ])),
              Container(
                margin: EdgeInsets.only(bottom: 50),
                child: RaisedButton(
                  child: Text('Generate password'),
                  onPressed: () {
                    generatePassword();
                  },
                ),
              ),
              GestureDetector(
                onTap: () {
                  if (password != "#" * passwordLength &&
                      !isGeneratingPassword) {
                    Clipboard.setData(ClipboardData(text: password));
                    showToast("Password copied to clipboard!");
                  }
                },
                child: Text(password,
                    style: TextStyle(
                        fontSize: 32,
                        fontFamily: 'Monospace',
                        color: Colors.black54,
                        fontWeight: FontWeight.bold)),
              )
            ])));
  }
}
